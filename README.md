# fastBoot

## Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Messaging with Redis](https://spring.io/guides/gs/messaging-redis/)
* [Quick Start](https://github.com/mybatis/spring-boot-starter/wiki/Quick-Start)
* [Accessing data with MySQL](https://spring.io/guides/gs/accessing-data-mysql/)

### 快速启动
1. 环境准备：JDK 1.8、Maven 3.6.1、MySQL 5.7、redis
2. 启动mysql：导入`src/main/resources/db/bms.sql`
3. 配置文件: `/src/main/resources/config/application-*.yml`
    - 配置数据源
    - 配置redis
4. 打包`mvn clean package -U -Dbuild.version=1.0.${BUILD_SID} -Dmaven.test.skip=true`
5. 启动`./bin/server.sh start test`

### 技术选型
- jdk 1.8
- hikari dataSource pool
- spring-boot 2.1.6
- mybatis-spring-boot-starter 2.0.1
- swagger-spring-boot-starter 1.9.0
