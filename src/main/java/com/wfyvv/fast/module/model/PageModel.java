package com.wfyvv.fast.module.model;

import java.io.Serializable;

public class PageModel<T> implements Serializable {

    private static final long    serialVersionUID = 6021983160740249951L;
    private              Integer page;
    private              Integer limit;

    public Integer getPage() {
        return page;
    }

    public T setPage(Integer page) {
        this.page = page;
        return (T) this;
    }

    public Integer getLimit() {
        return limit;
    }

    public T setLimit(Integer limit) {
        this.limit = limit;
        return (T) this;
    }
}
