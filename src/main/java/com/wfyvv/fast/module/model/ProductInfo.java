package com.wfyvv.fast.module.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductInfo implements Serializable {
    private static final long serialVersionUID = -9087274568371954107L;
    private Integer id;

    private String name;

    private String code;

    private String upgradeTime;

    private String ico;

    private Integer type;

    private Integer status;

    private String creator;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private String description;

    private String version;
}
