package com.wfyvv.fast.module.model;

import com.wfyvv.fast.common.core.constant.enums.Status;
import lombok.Builder;
import lombok.experimental.Accessors;

/**
 * 应用搜索条件
 *
 * @author fywang8
 */
@Builder
@Accessors(chain = true)
public class ProductQuery extends PageModel<ProductQuery> {

    private static final long serialVersionUID = -867647895019791543L;

    private String name;

    private String code;

    private Integer type;

    private Status status;
}
