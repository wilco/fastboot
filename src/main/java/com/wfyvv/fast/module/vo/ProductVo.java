package com.wfyvv.fast.module.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
public class ProductVo implements Serializable {
    private static final long    serialVersionUID = 4521528594784536864L;
    private              Integer id;
    private              String  nickName;
    private              String  name;
    private              String  description;
    private              Date    createTime;
    private              String  ico;
    private              String  module;
}
