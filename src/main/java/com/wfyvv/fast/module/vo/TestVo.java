package com.wfyvv.fast.module.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Accessors(chain = true)
public class TestVo implements Serializable {

    private static final long serialVersionUID = 7395851704447945111L;
    /**
     * 测试
     */
    @NotNull(message = "param[t] should not be null!")
    String t;


}
