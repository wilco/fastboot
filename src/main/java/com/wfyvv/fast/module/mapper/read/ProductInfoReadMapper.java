package com.wfyvv.fast.module.mapper.read;

import com.wfyvv.fast.module.model.ProductInfo;
import com.wfyvv.fast.module.model.ProductQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

@Mapper
public interface ProductInfoReadMapper {

    /**
     * 应用详情
     */
    @Select({
            "<script>",
            "select",
            "id, name, code, ico, type, status, creator, create_time, update_time, ",
            "description, upgrade_time",
            "from product_info",
            "where (status = 0 or status = 1)",
            "and id = #{id,jdbcType=INTEGER}",
            "</script>"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "name", property = "name", jdbcType = JdbcType.VARCHAR),
            @Result(column = "code", property = "code", jdbcType = JdbcType.VARCHAR),
            @Result(column = "ico", property = "ico", jdbcType = JdbcType.VARCHAR),
            @Result(column = "type", property = "type", jdbcType = JdbcType.INTEGER),
            @Result(column = "status", property = "status", jdbcType = JdbcType.INTEGER),
            @Result(column = "creator", property = "creator", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "description", property = "description", jdbcType = JdbcType.LONGVARCHAR),
            @Result(column = "upgrade_time", property = "upgradeTime", jdbcType = JdbcType.VARCHAR),
    })
    ProductInfo selectProductById(Integer id);


    /**
     * 根据唯一值查询应用列表
     */
    @Select({
            "<script>",
            "select",
            "id, name, code, ico, type, status, creator, create_time, update_time, ",
            "description, upgrade_time",
            "from product_info",
            "where 1=1 and (status = 0 or status = 1)",
            "and code = #{code, jdbcType=VARCHAR}",
            "ORDER BY update_time DESC limit 1",
            "</script>"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "name", property = "name", jdbcType = JdbcType.VARCHAR),
            @Result(column = "code", property = "code", jdbcType = JdbcType.VARCHAR),
            @Result(column = "ico", property = "ico", jdbcType = JdbcType.VARCHAR),
            @Result(column = "type", property = "type", jdbcType = JdbcType.INTEGER),
            @Result(column = "status", property = "status", jdbcType = JdbcType.INTEGER),
            @Result(column = "creator", property = "creator", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "description", property = "description", jdbcType = JdbcType.LONGVARCHAR),
            @Result(column = "upgrade_time", property = "upgradeTime", jdbcType = JdbcType.VARCHAR)
    })
    ProductInfo selectByCode(String code);

    /**
     * 应用列表
     *
     * @author fengyu.wang
     * @since 2019-06-30 16:02:15
     */
    @Select({
            "<script>",
            "select",
            "id, name, code, ico, type, status, creator, create_time, update_time, ",
            "description, upgrade_time",
            "FROM  product_info",
            "WHERE 1 = 1 ",
            "and status &lt; 2 ",
            "<if test='name != null and name != &apos;&apos;' > and (name like CONCAT('%',#{name, jdbcType=VARCHAR},'%') or  code like CONCAT('%',#{name, jdbcType=VARCHAR},'%')) </if>",
            "<if test='type != null'> and type = #{type,jdbcType=INTEGER} </if>",
            "<if test='status != null'> and status = #{status,jdbcType=INTEGER} </if>",
            "order by update_time DESC",
            "</script>"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
            @Result(column = "name", property = "name", jdbcType = JdbcType.VARCHAR),
            @Result(column = "code", property = "code", jdbcType = JdbcType.VARCHAR),
            @Result(column = "ico", property = "ico", jdbcType = JdbcType.VARCHAR),
            @Result(column = "type", property = "type", jdbcType = JdbcType.INTEGER),
            @Result(column = "status", property = "status", jdbcType = JdbcType.INTEGER),
            @Result(column = "creator", property = "creator", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "description", property = "description", jdbcType = JdbcType.LONGVARCHAR),
            @Result(column = "upgrade_time", property = "upgradeTime", jdbcType = JdbcType.VARCHAR)
    })
    List<ProductInfo> selectProductList(ProductQuery productQuery);
}
