package com.wfyvv.fast.module.mapper.write;

import com.wfyvv.fast.module.model.ProductInfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface ProductInfoMapper {

    /**
     * 新增应用
     */
    @Insert({
            "insert into product_info (name, ",
            "code, ico, ",
            "type, upgrade_time,",
            "creator, create_time, ",
            "update_time, description) ",
            "values (#{name,jdbcType=VARCHAR}, ",
            "#{code,jdbcType=VARCHAR}, #{ico,jdbcType=VARCHAR}, ",
            "#{type,jdbcType=INTEGER}, #{upgradeTime,jdbcType=VARCHAR},",
            "#{creator,jdbcType=VARCHAR}, #{createTime,jdbcType=TIMESTAMP}, ",
            "#{updateTime,jdbcType=TIMESTAMP}, #{description,jdbcType=LONGVARCHAR})"
    })
    int insert(ProductInfo record);


    /**
     * 更新应用
     */
    @Update({
            "<script>",
            "update product_info",
            "<set >",
            "<if test='name != null' > name = #{name, jdbcType=VARCHAR}, </if>",
            "<if test='code != null' > code = #{code, jdbcType=VARCHAR}, </if>",
            "<if test='ico != null' > ico = #{ico, jdbcType=VARCHAR}, </if>",
            "<if test='creator != null' > creator = #{creator, jdbcType=VARCHAR}, </if>",
            "<if test='updateTime != null' > update_time = #{updateTime,jdbcType=TIMESTAMP}, </if>",
            "<if test='description != null' > description = #{description, jdbcType=LONGVARCHAR}, </if>",
            "<if test='type != null' > type = #{type, jdbcType=INTEGER}, </if>",
            " upgrade_time = #{upgradeTime, jdbcType=VARCHAR}, ",
            "</set>",
            "where id = #{id, jdbcType=INTEGER}",
            "</script>"
    })
    int updateProductInfo(ProductInfo record);

    /**
     * 应用禁用与启用
     */
    @Update({
            "<script>",
            "update product_info",
            "set",
            "status  = #{status, jdbcType=INTEGER},",
            "update_time = now()",
            "where id = #{id,jdbcType=INTEGER} ",
            "</script>"
    })
    int updateProductStatus(@Param("id") int id, @Param("status") Integer status);
}
