package com.wfyvv.fast.module.controller;

import com.github.pagehelper.PageInfo;
import com.wfyvv.fast.common.core.Result;
import com.wfyvv.fast.common.core.ResultGenerator;
import com.wfyvv.fast.common.core.constant.enums.Status;
import com.wfyvv.fast.common.core.valid.Assert;
import com.wfyvv.fast.module.model.ProductInfo;
import com.wfyvv.fast.module.model.ProductQuery;
import com.wfyvv.fast.module.service.ProductInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

/**
 * product controller
 *
 * @author fengyu.wang
 * @since 2019-06-30 22:05:38
 */
@Slf4j
@AllArgsConstructor
@Api(tags = "应用管理")
@RestController
public class ProductInfoController {

    private static final String PRODUCT = "/product";
    private static final String PRODUCT_LISTS = "/products";
    private static final String PRODUCT_BY_ID = "/product/{id}";
    private static final String PRODUCT_ENABLE = "/product/enable/{id}";
    private static final String PRODUCT_DISABLE = "/product/disable/{id}";

    private final ProductInfoService productInfoService;

    @PostMapping("PRODUCT_URL")
    @ApiOperation("新增应用")
    public Result addProductInfo(
            @ApiParam(value = "应用名称", required = true) @RequestParam("name") String name,
            @ApiParam(value = "应用编码", required = true) @RequestParam("code") String code,
            @ApiParam(value = "缩略图", required = true) @RequestParam("ico") String ico,
            @ApiParam(value = "应用更新时间段") @RequestParam(value = "upgradeTime", required = false, defaultValue = "") String upgradeTime,
            @ApiParam(value = "应用简介") @RequestParam(value = "description", required = false, defaultValue = "") String description,
            @ApiParam(value = "类型") @RequestParam(value = "type", required = false, defaultValue = "1") int type
    ) {
        log.info("addProductInfo : [name:{}, code:{}] ", name, code);
        //新增之前判断该应用是否存在
        ProductInfo isExist = productInfoService.selectByCode(code);
        Assert.notNull(isExist, "应用编码已经存在");

        ProductInfo.ProductInfoBuilder productInfoBuilder = ProductInfo.builder().code(code)
                .name(name)
                .ico(ico)
                .type(type)
                .description(description)
                .creator("test")
                .createTime(LocalDateTime.now())
                .updateTime(LocalDateTime.now());
        //判断应用更新时间段是否合理
        if (StringUtils.isNotBlank(upgradeTime)) {
            Assert.assertTrue(checkTimeFormat(upgradeTime), "更新时间段时间格式有误");
            productInfoBuilder.upgradeTime(upgradeTime);
        }
        if (productInfoService.insertProductInfo(productInfoBuilder.build()) > 0) {
            return ResultGenerator.genSuccessResult(true);
        }
        return ResultGenerator.genFailResult();
    }


    @PutMapping(PRODUCT)
    @ApiOperation("更新应用")
    public Result updateProductInfo(
            @ApiParam(value = "应用id", required = true) @RequestParam("id") int id,
            @ApiParam(value = "应用名称", required = true) @RequestParam("name") String name,
            @ApiParam(value = "应用编码", required = true) @RequestParam("code") String code,
            @ApiParam(value = "缩略图", required = true) @RequestParam("ico") String ico,
            @ApiParam(value = "应用简介") @RequestParam(value = "description", required = false, defaultValue = "") String description,
            @ApiParam(value = "应用更新时间段") @RequestParam(value = "upgradeTime", required = false, defaultValue = "") String upgradeTime,
            @ApiParam(value = "类型") @RequestParam(value = "type", required = false) int type
    ) {
        log.info("updateProductInfo : [name:{}, code:{}] ", name, code);
        int count = productInfoService.updateProductInfo(ProductInfo.builder()
                .id(id)
                .name(name)
                .code(code)
                .ico(ico)
                .updateTime(LocalDateTime.now())
                .description(description)
                .upgradeTime(upgradeTime)
                .type(type).build());
        if (count > 0) {
            return ResultGenerator.genSuccessResult();
        }
        return ResultGenerator.genFailResult();
    }

    @GetMapping(PRODUCT_LISTS)
    @ApiOperation("应用列表")
    public Result getProductList(
            @ApiParam(value = "应用名或编码") @RequestParam(value = "name", required = false, defaultValue = "") String name,
            @ApiParam(value = "应用状态") @RequestParam(value = "status", required = false) Integer status,
            @ApiParam(value = "应用类型") @RequestParam(value = "type", required = false) Integer type,
            @ApiParam(value = "第几页") @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @ApiParam(value = "每页数") @RequestParam(value = "limit", required = false, defaultValue = "10") int limit
    ) {
        log.info("getProductList : [page:{}, limit:{}] ", page, limit);
        ProductQuery productQuery = ProductQuery.builder()
                .name(name)
                .code(name)
                .status(Status.valueOf(status))
                .type(type).build();
        PageInfo<ProductInfo> productList = productInfoService.selectProductList(productQuery.setPage(page).setLimit(limit));
        return ResultGenerator.genSuccessPageResult(productList.getList(), productList.getTotal());
    }


    @GetMapping(PRODUCT_BY_ID)
    @ApiOperation("应用详情")
    public Result selectProductById(@ApiParam(value = "应用id") @PathVariable("id") int id) {
        log.info("selectProductById : [id:{}] ", id);
        ProductInfo detail = productInfoService.selectProductById(id);
        return ResultGenerator.genSuccessResult(detail);
    }


    @DeleteMapping(PRODUCT_BY_ID)
    @ApiOperation("应用删除")
    public Result deleteProduct(@ApiParam(value = "应用id", required = true) @PathVariable("id") int id) {
        log.info("deleteProduct : [id:{}] ", id);
        int count = productInfoService.updateProductStatus(id, Status.DELETE);
        if (count > 0) {
            return ResultGenerator.genSuccessResult();
        }
        return ResultGenerator.genFailResult();
    }

    @PutMapping(PRODUCT_ENABLE)
    @ApiOperation("应用启用")
    public Result enableProduct(@ApiParam(value = "应用id", required = true) @PathVariable("id") int id) {
        log.info("enableProduct : [id:{}] ", id);
        //启动应用
        int count = productInfoService.updateProductStatus(id, Status.NORMAL);
        if (count > 0) {
            return ResultGenerator.genSuccessResult();
        }
        return ResultGenerator.genFailResult();
    }

    @PutMapping(PRODUCT_DISABLE)
    @ApiOperation("应用禁用")
    public Result disableProduct(@ApiParam(value = "应用id", required = true) @PathVariable("id") int id) {
        log.info("disableProduct : [id:{}] ", id);
        int count = productInfoService.updateProductStatus(id, Status.DISABLE);
        if (count > 0) {
            return ResultGenerator.genSuccessResult();
        }
        return ResultGenerator.genFailResult();
    }

    private Boolean checkTimeFormat(String str) {
        //TODO
        return true;
    }
}

