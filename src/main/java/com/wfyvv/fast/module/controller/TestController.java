package com.wfyvv.fast.module.controller;

import com.wfyvv.fast.common.core.Result;
import com.wfyvv.fast.common.core.ResultGenerator;
import com.wfyvv.fast.common.core.valid.Assert;
import com.wfyvv.fast.common.core.valid.ValidatorUtil;
import com.wfyvv.fast.module.vo.TestVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "Test")
@RestController
@RequestMapping("/t")
@Slf4j
public class TestController {

    @GetMapping
    @ApiOperation(value = "默认查询", notes = "查询版本")
    public Result version() {
        return ResultGenerator.genSuccessResult("v1.0");
    }

    @GetMapping("/h")
    public Result hello(@ApiParam("用户名") @RequestParam("name")String name) {
        Assert.isBlank(name, "name can not be null!");
        return ResultGenerator.genSuccessResult(name + " Hello World！");
    }

    @GetMapping("/o")
    public Result object(TestVo testVo) {
        ValidatorUtil.validateEntity(testVo);
        return ResultGenerator.genSuccessResult(testVo);
    }

    @GetMapping("/l")
    public Result list() {
        TestVo testVo1 = new TestVo();
        testVo1.setT("test vo 1");
        TestVo testVo2 = new TestVo();
        testVo2.setT("test vo 2");
        List<TestVo> list = new ArrayList<>();
        list.add(testVo1);
        list.add(testVo2);
        return ResultGenerator.genSuccessPageResult(list);
    }

    @GetMapping("/test")
    public Result getExampleTxt() {
        return ResultGenerator.genErrorResult("<h2 style='color:red'>中文乱码</h2>");
    }
}
