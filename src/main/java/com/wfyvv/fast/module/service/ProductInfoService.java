package com.wfyvv.fast.module.service;

import com.github.pagehelper.PageInfo;
import com.wfyvv.fast.common.core.constant.enums.Status;
import com.wfyvv.fast.module.model.ProductInfo;
import com.wfyvv.fast.module.model.ProductQuery;

public interface ProductInfoService {

    PageInfo<ProductInfo> selectProductList(ProductQuery productQuery);

    /**
     * 新增应用
     */
    int insertProductInfo(ProductInfo productInfo);

    /**
     * 应用详情
     */
    ProductInfo selectProductById(Integer id);

    /**
     * 更新应用
     */
    int updateProductInfo(ProductInfo productInfo);

    /**
     * 更新应用状态
     */
    int updateProductStatus(Integer id, Status status);

    /**
     * 根据code查询应用详情
     */
    ProductInfo selectByCode(String code);
}
