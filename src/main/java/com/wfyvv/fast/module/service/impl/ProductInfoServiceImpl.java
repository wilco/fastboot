package com.wfyvv.fast.module.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wfyvv.fast.common.core.constant.enums.Status;
import com.wfyvv.fast.module.mapper.read.ProductInfoReadMapper;
import com.wfyvv.fast.module.mapper.write.ProductInfoMapper;
import com.wfyvv.fast.module.model.ProductInfo;
import com.wfyvv.fast.module.model.ProductQuery;
import com.wfyvv.fast.module.service.ProductInfoService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@AllArgsConstructor
public class ProductInfoServiceImpl implements ProductInfoService {

    private final ProductInfoMapper productInfoMapper;
    private final ProductInfoReadMapper productInfoReadMapper;

    @Override
    public PageInfo<ProductInfo> selectProductList(ProductQuery productQuery) {
        PageHelper.startPage(productQuery.getPage(), productQuery.getLimit());
        List<ProductInfo> productList = productInfoReadMapper.selectProductList(productQuery);
        return new PageInfo<>(productList);
    }

    @Override
    public int insertProductInfo(ProductInfo productInfo) {
        return productInfoMapper.insert(productInfo);
    }

    @Override
    public ProductInfo selectProductById(Integer id) {
        return productInfoReadMapper.selectProductById(id);
    }

    @Override
    public int updateProductInfo(ProductInfo productInfo) {
        return productInfoMapper.updateProductInfo(productInfo);
    }

    @Override
    public int updateProductStatus(Integer id, Status status) {
        return productInfoMapper.updateProductStatus(id, status.getStatus());
    }

    @Override
    public ProductInfo selectByCode(String code) {
        return productInfoReadMapper.selectByCode(code);
    }
}

