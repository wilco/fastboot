package com.wfyvv.fast.common.core.constant.enums;

import lombok.Getter;

@Getter
public enum ProductType {
    /**
     * 主应用
     */
    MAIN("0"),
    /**
     * 副应用
     */
    NORMAL("1"),
    /**
     * 模块应用
     */
    MODULE("2");

    private String type;

    ProductType(String type) {
        this.type = type;
    }
}
