package com.wfyvv.fast.common.core;

import java.io.Serializable;
import java.util.List;

public class ResultGenerator {
    private ResultGenerator() {
    }

    private static final String DEFAULT_SUCCESS_MESSAGE = "SUCCESS";
    private static final String DEFAULT_FAIL_MESSAGE = "ERROR";

    public static Result genSuccessResult() {
        return new Result()
                .setCode(Result.ResultCode.SUCCESS)
                .setMessage(DEFAULT_SUCCESS_MESSAGE);
    }

    public static <T extends Serializable> Result genSuccessResult(T data) {
        return new Result<T>()
                .setCode(Result.ResultCode.SUCCESS)
                .setMessage(DEFAULT_SUCCESS_MESSAGE)
                .setData(data);
    }

    public static Result genFailResult() {
        return new Result()
                .setCode(Result.ResultCode.FAIL)
                .setMessage(DEFAULT_FAIL_MESSAGE);
    }

    public static Result genFailResult(String message) {
        return new Result()
                .setCode(Result.ResultCode.FAIL)
                .setMessage(message);
    }

    public static Result genUnauthorizedResult(String message) {
        return new Result()
                .setCode(Result.ResultCode.UNAUTHORIZED)
                .setMessage(message);
    }

    public static Result genNotFoundResult(String message) {
        return new Result()
                .setCode(Result.ResultCode.NOT_FOUND)
                .setMessage(message);
    }

    public static Result genErrorResult(String message) {
        return new Result()
                .setCode(Result.ResultCode.INTERNAL_SERVER_ERROR)
                .setMessage(message);
    }

    public static <T extends Serializable> Result genSuccessPageResult(List<T> data) {
        return new Result<ResultPage<T>>()
                .setCode(Result.ResultCode.SUCCESS)
                .setMessage(DEFAULT_SUCCESS_MESSAGE)
                .setData(new ResultPage<T>().setData(data).setTotal(data.size()));
    }

    public static <T extends Serializable> Result genSuccessPageResult(List<T> data, long total) {
        return new Result<ResultPage<T>>()
                .setCode(Result.ResultCode.SUCCESS)
                .setMessage(DEFAULT_SUCCESS_MESSAGE)
                .setData(new ResultPage<T>().setData(data).setTotal(total));
    }

}

