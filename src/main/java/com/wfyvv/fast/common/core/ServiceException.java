package com.wfyvv.fast.common.core;

public class ServiceException extends RuntimeException {
    private static final long serialVersionUID = 5909602855564978590L;

    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
