package com.wfyvv.fast.common.core.constant;

public final class Constant {

    private Constant() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * 超级管理员角色id
     */
    public static final Integer ADMIN_ROLE_ID = 1;
}
