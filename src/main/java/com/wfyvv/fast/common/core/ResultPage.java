package com.wfyvv.fast.common.core;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class ResultPage<T extends Serializable> implements Serializable {
    private static final long    serialVersionUID = 4807407925777076607L;
    private              long    total;
    private              List<T> data;

    public ResultPage() {
        total = 0L;
        data = Collections.emptyList();
    }

    public long getTotal() {
        return total;
    }

    public ResultPage<T> setTotal(long total) {
        this.total = total;
        return this;
    }

    public List<T> getData() {
        return data;
    }

    public ResultPage<T> setData(List<T> data) {
        this.data = data;
        return this;
    }
}
