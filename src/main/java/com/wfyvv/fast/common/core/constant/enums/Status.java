package com.wfyvv.fast.common.core.constant.enums;

import lombok.Getter;

@Getter
public enum Status {
    /**
     * 启用
     */
    NORMAL(0),
    /**
     * 禁用
     */
    DISABLE(1),
    /**
     * 删除
     */
    DELETE(2);
    private Integer status;

    Status(Integer status) {
        this.status = status;
    }

    public static Status valueOf(Integer status) {
        if (status == null) {
            return null;
        } else {
            for (Status s : Status.values()) {
                if (s.getStatus().equals(status)) {
                    return s;
                }
            }
            return null;
        }
    }
}
