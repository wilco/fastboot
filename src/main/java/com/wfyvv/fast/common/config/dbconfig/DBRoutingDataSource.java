package com.wfyvv.fast.common.config.dbconfig;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

public class DBRoutingDataSource extends AbstractRoutingDataSource {
    @Override
    protected Object determineCurrentLookupKey() {
        return DBContextHolder.get();
    }
}
