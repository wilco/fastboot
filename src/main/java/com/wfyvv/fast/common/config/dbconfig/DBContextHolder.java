package com.wfyvv.fast.common.config.dbconfig;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DBContextHolder {
    private static ThreadLocal<DBTypeEnum> contextHolder = new ThreadLocal<>();

    public static void set(DBTypeEnum dbType) {
        contextHolder.set(dbType);
    }

    public static DBTypeEnum get() {
        return contextHolder.get();
    }

    public static void master() {
        set(DBTypeEnum.MASTER);
        log.debug("DBContextHolder.master: db switch to master");
    }

    public static void slave() {
        set(DBTypeEnum.SLAVE);
        log.debug("DBContextHolder.slave: db switch to slave");
    }
}
