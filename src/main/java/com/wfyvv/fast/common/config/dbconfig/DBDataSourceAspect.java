package com.wfyvv.fast.common.config.dbconfig;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Order(-1)
@Component
public class DBDataSourceAspect {
    @Pointcut("execution(* com.wfyvv.fast.module.mapper.read.*.*(..))")
    public void readPointcut() {

    }

    @Pointcut("execution(* com.wfyvv.fast.module.mapper.write.*.*(..))")
    public void writePointcut() {

    }


    @Before("readPointcut()")
    public void read() {
        log.debug("DBDataSourceAspect.read: current operation read db.");
        DBContextHolder.slave();
    }

    @Before("writePointcut()")
    public void write() {
        log.debug("DBDataSourceAspect.write: current operation write db.");
        DBContextHolder.master();
    }
}
