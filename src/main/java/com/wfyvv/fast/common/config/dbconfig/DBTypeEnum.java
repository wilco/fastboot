package com.wfyvv.fast.common.config.dbconfig;

import lombok.Getter;

public enum DBTypeEnum {
    /**
     * Master DB for Read/Write
     */
    MASTER(0, "master"),

    /**
     * Slave DB for Read
     */
    SLAVE(1, "slave");

    @Getter
    private int code;

    @Getter
    private String name;

    DBTypeEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }
}
