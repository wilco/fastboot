package com.wfyvv.fast.common.utils;

public class BeanUtils {
    private BeanUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static <T> T copyProperties(Object source, T target) {
        org.springframework.beans.BeanUtils.copyProperties(source, target);
        return target;
    }
}
