package com.wfyvv.fast;

import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableSwagger2Doc
@EnableCaching(proxyTargetClass = true)
@SpringBootApplication
public class FastBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(FastBootApplication.class, args);
	}
}
