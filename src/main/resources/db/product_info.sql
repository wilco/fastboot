/*
 Navicat Premium Data Transfer

 Source Server         : 212.64.2.218
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : 212.64.2.218:3308
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 13/07/2019 16:37:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for product_info
-- ----------------------------
DROP TABLE IF EXISTS `product_info`;
CREATE TABLE `product_info`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '描述信息',
  `ico` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `type` tinyint(4) NOT NULL DEFAULT 1 COMMENT '应用类型：默认为1；主应用为0',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0:正常 1：停用 -1：删除',
  `creator` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人',
  `upgrade_time` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '升级时间',
  `create_time` timestamp(0) NULL DEFAULT NULL,
  `update_time` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_info
-- ----------------------------
INSERT INTO `product_info` VALUES (1, '应用1', '1001', 'test', 'https://static.wfyvv.com/avatar.png', 1, 2, 'test', NULL, '2019-06-30 09:03:50', '2019-07-07 14:51:07');
INSERT INTO `product_info` VALUES (2, '应用2', '1002', 'test', 'https://static.wfyvv.com/avatar.png', 1, 1, 'test', NULL, '2019-06-30 09:14:11', '2019-06-30 09:14:11');
INSERT INTO `product_info` VALUES (3, '应用3', '1003', 'test', 'https://static.wfyvv.com/avatar.png', 1, 1, 'test', '10:00-12:00', '2019-06-30 14:10:09', '2019-06-30 14:10:09');
INSERT INTO `product_info` VALUES (4, '应用4', '1004', 'test', 'https://static.wfyvv.com/avatar.png', 1, 1, 'test', '10:00-12:00', '2019-06-30 14:10:09', '2019-06-30 14:10:09');

SET FOREIGN_KEY_CHECKS = 1;
